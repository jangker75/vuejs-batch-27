// SOAL 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for (let index = 0; index < daftarHewan.length; index++) {
    console.log(daftarHewan.sort()[index]);
}

// SOAL 2
function introduce(_object) {
    return "Nama saya "+_object['name']+", umur saya "+_object['age'].toString()+" tahun, alamat saya di "+_object['address']+", dan saya punya hobby yaitu "+_object['hobby']
}

var data = {name : "Taufik" , age : 28 , address : "Jakarta Utara" , hobby : "Gaming" }
// var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

//SOAL 3
function hitung_huruf_vokal(params) {
    kata = params.toLowerCase()
    console.log(kata)
    huruf_vokal = ['a','i','u','e','o']
    var count = 0
    for (let index = 0; index < kata.length; index++) {
        if (huruf_vokal.includes(kata[index])) {
            count++
        }
    }
    return count
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
var hitung_3 = hitung_huruf_vokal("Taufik Hidayatullah")
console.log(hitung_1 , hitung_2, hitung_3) // 3 2

// SOAL 4
function hitung(params) {
    u1 = 0
    b = 2
    return u1 + (params - 1)*b
}
console.log( hitung(-2) ) // -6
console.log( hitung(-1) ) // -4
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(4) ) // 6
console.log( hitung(5) ) // 8