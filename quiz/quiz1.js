// SOAL 1
function jumlah_kata(params) {
    console.log(params.trim().split(' ').length)        
}
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
jumlah_kata(kalimat_1)
jumlah_kata(kalimat_2)

// SOAL 2
function conv_bulan(params) {
    
    switch (params) {
        case 1:
            bulan = 'Januari'
            break;
        case 2:
            bulan = 'Februari'
            break;
        case 3:
            bulan = 'Maret'
            break;
        case 4:
            bulan = 'April'
            break;
        case 5:
            bulan = 'Mei'
            break;
        case 6:
            bulan = 'Juni'
            break;
        case 7:
            bulan = 'Juli'
            break;
        case 8:
            bulan = 'Agustus'
            break;
        case 9:
            bulan = 'September'
            break;
        case 10:
            bulan = 'Oktober'
            break;
        case 11:
            bulan = 'November'
            break;
    
        default:
            bulan = 'Desember'
            break;
    }
    return bulan
}

function total_hari(bulan, tahun) {
    hari_31 = [1,3,5,7,8,10,12]
    hari_30 = [4,6,9,11]
    if (hari_31.includes(bulan)) {
        jml_hari = 31
    } else if(hari_30.includes(bulan)) {
        jml_hari = 30
    }
    else if((tahun % 4) ==0 ){
        jml_hari = 29
    }else{
        jml_hari = 28
    }
    return jml_hari
}
function next_date(tgl, bulan, tahun) {
    
    tgl++
    if (tgl>total_hari(bulan, tahun)) {
        tgl = tgl % total_hari(bulan, tahun)
        bulan++
        if(bulan > 12){
            bulan = bulan % 12
            tahun++
        }
    }
    var nextdate = tgl.toString()+' '+conv_bulan(bulan)+' '+tahun.toString()
    console.log( nextdate)
}
var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun )

var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal , bulan , tahun )

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun )