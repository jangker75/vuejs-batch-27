// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var k1 = pertama.substr(0,4);
var k2 = pertama.substr(12,6);
var k3 = kedua.substr(0,7);
var k4 = kedua.substr(8,10);
var space = ' ';
console.log('=== Soal 1 ===');
console.log(k1+space+k2+space+k3+space+k4.toUpperCase());

// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
console.log('=== Soal 2 ===');
console.log((parseInt(kataPertama)+parseInt(kataKeempat))+(parseInt(kataKedua)*parseInt(kataKetiga)));

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('=== Soal 3 ==='); 
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);