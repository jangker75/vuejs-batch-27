// SOAL 1
const luasPersegiPanjang = (p, l) => {
    return p*l
}
const kelilingPersegiPanjang = (p, l) => {
    return 2*(p+l)
}

console.log(luasPersegiPanjang(3, 5))
console.log(kelilingPersegiPanjang(3, 5))

// SOAL 2
const newFunction  = (firstName, lastName) => {
    return {firstName},
    {lastName}, 
    {fullName : ()=>{ 
        console.log(`${firstName} ${lastName}`)
    }}

  }
   
  //Driver Code 
newFunction("William", "Imoh").fullName() 

// SOAL 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName,lastName,address,hobby} = newObject
  //Driver Code 
console.log(firstName, lastName, address, hobby)

// SOAL 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


// SOAL 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
var after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`
console.log(before)
console.log(after)